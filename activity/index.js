//console.log("Hello World!");

// 3.
let carBrands = ["Mitsubishi", "Toyota", "Honda", "Chevrolet", "Nissan", "Hyundai"];
console.log("Original Brand List:");
console.log(carBrands);

function addBrand(brand) {
    carBrands.push(brand);
}

addBrand("Ford");
console.log("Edited Brand List:");
console.log(carBrands);

// 4.
function getNewBrand(index) {
    return carBrands[index];
}

let itemFound = getNewBrand(4);
console.log("Car brand found: " + itemFound);

// 5.
function deleteLastBrand() {
    let LastBrand = carBrands[carBrands.length - 1];
    carBrands.pop();
    return LastBrand;
}

let lastBrand = deleteLastBrand();
console.log("Last brand before the deletion was: " + lastBrand);

//6.
function updateCarBrand(index, brand) {
    carBrands[index] = brand;
}

updateCarBrand(1, "Audi");
console.log(carBrands);

//7.
function deleteBrand() {
    carBrands = [];
}

deleteBrand();
console.log(carBrands);

// 8.
function checkArr() {
    if (carBrands.length > 0) {
        return false;
    } else {
        return true;
    }
}

let isUserEmpty = checkArr();
console.log(isUserEmpty);