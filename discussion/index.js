// console.log("Hello World!");

let studentNumberA = "2022-0001";
let studentNumberB = "2022-0002";
let studentNumberC = "2022-0003";
let studentNumberD = "2022-0004";
let studentNumberE = "2022-0005";

console.log(studentNumberA);
console.log(studentNumberB);
console.log(studentNumberC);
console.log(studentNumberD);
console.log(studentNumberE);

let studentID = ["2022-0001", "2022-0002", "2022-0003", "2022-0004", "2022-0005"];

console.log(studentID);

/*

	Arrays
		- used to store multiple related values in a single variable.
		- this is also declared using the square bracket([]) also known as "Array literals."

		Syntax:
		let/const arrayName = [elementA, elementB, ... elementZ]

*/

let grades = [96.8, 85.7, 93.2, 94.6];
console.log(grades);
let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu"];
console.log(computerBrands);

let mixedArr = [12, "Asus", null, undefined, {}];
console.log(mixedArr);

let myTasks = [
	"drink html",
	"eat JavaScript",
	"inhale css",
	"bake sass"
];

console.log(myTasks);

let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "New York";
let city4 = "Beijing";

let cities = [city1, city2, city3, city4];
let citiesSample = ["Tokyo", "Manila", "New York", "Beijing"];

// Objects are declared using curly braces ({});


console.log(cities);
/*console.log(citiesSample);
console.log(cities.trim === citiesSample.trim);*/


// Array Length Property
/*
	- to set or get the items or elements in an array
*/

console.log(myTasks.length); // 4
console.log(cities.length); // 4

let blankArr = [];
console.log(blankArr.length);

let fullName = "Fidel Ramos";
console.log(fullName.length); // 11

myTasks.length--
console.log(myTasks.length); // 3
console.log(myTasks);

let theBeatles = ["John", "Paul", "Ringo", "George"];
console.log(theBeatles.length)
theBeatles.length++
console.log(theBeatles.length)
console.log(theBeatles);

theBeatles[4] = "Aronn";
console.log(theBeatles);

/*
	Accessing Elements of an Array

	Syntax:
		arrayName[index];
*/

console.log(grades[0]);
console.log(computerBrands[3]);

let lakerslegend = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"];
console.log(lakerslegend[0]);
console.log(lakerslegend[2]);


let currentLaker = lakerslegend[2];
console.log(currentLaker);


console.log("Array before reassignment;");
console.log(lakerslegend);
lakerslegend[2] = "Pau Gasol";
console.log("Array after reassignment:")
console.log(lakerslegend);

const bullsLegend = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];

let lastElementIndex = bullsLegend.length -1
console.log(bullsLegend.length);
console.log(bullsLegend[lastElementIndex]);



// Adding items into an array
let newArr = [];
console.log(newArr);
console.log(newArr[0]);
newArr[0] = "Jenny";
console.log(newArr);
newArr[1] = "Jisoo";
console.log(newArr);

newArr[newArr.length++] = "Lisa";
console.log(newArr);

/*
Mini activity:

Part 1: Adding a value at the end of the array

	-Create a function which is able to receive a single argument and add the input at the end of the superheroes array
	-Invoke and add an argument to be passed to the function
	-Log the superheroes array in the console
*/


// Code:

let superHeroes = ["Iron Man", "Spiderman", "Captain America"];
console.log(superHeroes);
function addsuperHero(hero){
	superHeroes[superHeroes.length++] = hero;
}

addsuperHero("Captain Marvel");
console.log(superHeroes);
addsuperHero("Dr. Strange");
console.log(superHeroes);


/*
Part 2: Retrieving an element using a function
	
	-Create a function which is able to receive an index number as a single argument
	-Return the element/item accessed by the index.
	-Create a global variable named heroFound and store/pass the value returned by the function.
	-Log the heroFound variable in the console.
*/


// Code:

function findHero(index){
	return superHeroes[index];
}

let heroFound = superHeroes[2];
console.log("Hero Found: " + heroFound);

for(let index = 0; index < newArr.length; index++) {
	console.log(newArr[index])
}

let numArr = [5, 12, 26, 30, 42, 50, 67, 85];
for (let index = 0; index < numArr.length; index++ ){
	if(numArr[index] % 5 === 0) {
		console.log(numArr[index] + " is divisible by 5.");
	} else {
		console.log(numArr[index] + " is not divisible by 5.");
	}
}

// Multidimensional Arrays

/*
	- arrays within an array
*/

let chessBoard = [
	["A1", "B1", "C1", "D1", "E1", "F1", "G1", "H1"], 
	["A2", "B2", "C2", "D2", "E2", "F2", "G2", "H2"], 
	["A3", "B3", "C3", "D3", "E3", "F3", "G3", "H3"], 
	["A4", "B4", "C4", "D4", "E4", "F4", "G4", "H4"], 
	["A5", "B5", "C5", "D5", "E5", "F5", "G5", "H5"], 
	["A6", "B6", "C6", "D6", "E6", "F6", "G6", "H6"], 
	["A7", "B7", "C7", "D7", "E7", "F7", "G7", "H7"], 
	["A8", "B8", "C8", "D8", "E8", "F8", "G8", "H8"] 
]

console.log(chessBoard);
console.log(chessBoard[1][4]);
console.log("Pawn moves to: " + chessBoard[7][4]);


